#UnitName
#   VisibleName <name>
#   Color <uint>
#   Parent <factionname>

#Relations
#   <factionname> <factionname> <relation>

Blues
{
    VisibleName Blue Guys
    Color 0xff0000ff
}

Reds
{
    VisibleName Red Guys
    Color 0xffff0000
}

Relations
{
    Blues Reds Enemy
}