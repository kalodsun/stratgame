﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Game.Map;
using UnityEngine;//only for Mathf

namespace Assets.Scripts.Game.TiledMapPathfinder
{
    class AStarPathfinder : IMapPathfinder
    {
        private readonly MapNode[,] graph;
        private readonly TiledMap map;
        private EdgeWeightDelegate edgeWeightDelegate;

        public delegate float EdgeWeightDelegate(MapTile from, MapTile to, TiledMap map);

        public AStarPathfinder(TiledMap map, EdgeWeightDelegate edgeWeightDelegate)
        {
            this.map = map;
            this.edgeWeightDelegate = edgeWeightDelegate;
            graph = new MapNode[map.Width, map.Height];

            for (int y = 0; y < map.Height; y++)
                for (int x = 0; x < map.Width; x++)
                    graph[x, y] = new MapNode();

            for(int y = 0;y < map.Height;y++)
                for (int x = 0; x < map.Width; x++)
                {
                    foreach (IntVector2 mod in MapTile.NeighbourSet)
                    {
                        if (!Utils.IsValidCoord(x + mod.x, map.Width) || !Utils.IsValidCoord(y + mod.y, map.Height))
                            continue;
                        graph[x, y].MakeEdge(graph[x + mod.x, y + mod.y]);
                        graph[x, y].X = x;
                        graph[x, y].Y = y;
                    }
                }
        }

        public void SetEdgeWeightDelegate(EdgeWeightDelegate edgeWeightDelegate)
        {
            this.edgeWeightDelegate = edgeWeightDelegate;
        }

        public void UpdateTile(MapTile tile)
        {
            throw new NotImplementedException();
            //TODO
        }

        public List<MapTile> FindPath(MapTile start, MapTile end)
        {
            var path = FindPath(graph[start.X, start.Y], graph[end.X, end.Y]);

            if (path == null)
                return null;

            var ret = new List<MapTile>(path.Count);

            foreach(MapNode node in path)
                ret.Add(map.Tiles[node.X, node.Y]);

            return ret;
        }

        private class FrontierComparer : IComparer<float>
        {
            public int Compare(float x, float y)
            {
                int result = x.CompareTo(y);

                if (result == 0)
                    return 1;   // Handle equality as beeing greater
                return result;
            }
        }

        private List<MapNode> FindPath(MapNode start, MapNode end)
        {
            var visited = new HashSet<MapNode>();
            var dist = new Dictionary<MapNode, float>();
            var prev = new Dictionary<MapNode, MapNode>();
            var frontier = new SortedList<float, MapNode>(new FrontierComparer());

            dist[start] = 0;
            frontier.Add(Mathf.Max(Mathf.Abs(start.X - end.X), Mathf.Abs(start.Y - end.Y)), start);

            while(frontier.Count > 0)
            {
                var current = frontier.Values[0];
                visited.Add(current);
                frontier.RemoveAt(0);

                if (current == end)
                    return ReconstrutPath(current, start, prev);

                foreach(var edge in current.Connections)
                {
                    MapNode endNode = edge.End;
                    if (visited.Contains(endNode))
                        continue;

                    float edist = edgeWeightDelegate(edge.GetStartTile(map), edge.GetEndTile(map), map);

                    if(edist < 0)
                        continue;

                    float cdist = dist[current] + edist;

                    if (frontier.Values.Contains(endNode) && cdist >= dist[endNode])
                        continue;

                    prev[endNode] = current;
                    dist[endNode] = cdist;

                    if (frontier.Values.Contains(endNode))
                        frontier.RemoveAt(frontier.Values.IndexOf(endNode));

                    float heurDist = Mathf.Max(Mathf.Abs(endNode.X - end.X), Mathf.Abs(endNode.Y - end.Y)) + dist[endNode];
                    frontier.Add(heurDist, endNode);
                }

            }

            return null;
        }

        private static List<MapNode> ReconstrutPath(MapNode end, MapNode start, Dictionary<MapNode, MapNode> prev)
        {
            var ret = new List<MapNode> {end};

            while (end != start)
            {
                end = prev[end];
                ret.Add(end);
            }

            ret.Reverse();

            return ret;
        }

        internal class MapNode
        {
            public int X, Y;
            public readonly List<MapEdge> Connections;

            public MapNode()
            {
                Connections = new List<MapEdge>();
            }

            public bool HasEdgeTo(MapNode node)
            {
                return Connections.Any(edge => edge.End == node);
            }

            public MapEdge GetEdge(MapNode node)
            {
                return Connections.FirstOrDefault(edge => edge.End == node);
            }

            public void MakeEdge(MapNode end)
            {
                Connections.Add(new MapEdge(this, end));
            }

            public void DeleteEdge(MapNode end)
            {
                Connections.Remove(GetEdge(end));
            }

            internal class MapEdge
            {
                public MapNode Start, End;

                public MapEdge(MapNode start, MapNode end)
                {
                    Start = start;
                    End = end;
                }

                public MapTile GetStartTile(TiledMap map)
                {
                    return map.Tiles[Start.X, Start.Y];
                }

                public MapTile GetEndTile(TiledMap map)
                {
                    return map.Tiles[End.X, End.Y];
                }
            }
        }
    }
}
