﻿using System.Collections.Generic;
using Assets.Scripts.Game.Map;
using JetBrains.Annotations;

namespace Assets.Scripts.Game.TiledMapPathfinder
{
    interface IMapPathfinder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>List of map tiles in the path, start and end inclusive.</returns>
        [CanBeNull]
        List<MapTile> FindPath(MapTile start, MapTile end);
    }
}
