﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Game.Entities
{
    public class Faction
    {
        public Faction Parent { get; set; }
        public uint Color { get; set; }
        public Dictionary<Faction, Relation> Relations { get; set; }
        public string Name { get; set; }
        public string VisibleName { get; set; }

        public Faction()
        {
            Parent = null;
            Color = 0xFFFFFFFF;
            Relations = new Dictionary<Faction, Relation>();
            Name = "";
            VisibleName = "";
        }

        public enum Relation
        {
            Ally, Neutral, Enemy
        }
    }
}
