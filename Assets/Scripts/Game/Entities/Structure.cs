﻿using Assets.Scripts.Game.Map;

namespace Assets.Scripts.Game.Entities
{
    public class Structure
    {
        public Structure(StatsCls stats)
        {
            Stats = stats;
            Properties = new PropertiesCls(stats);
            Tile = null;
        }

        private MapTile tile;

        public MapTile.Direction Direction { get; set; }

        public MapTile Tile
        {
            get { return tile; }
            private set
            {
                if (tile != null)
                    tile.Structures.Remove(this);
                tile = value;
                if (tile != null)
                    tile.Structures.Add(this);
            }
        }

        public PropertiesCls Properties { private set; get; }
        public StatsCls Stats { private set; get; }

        public void SetTile(MapTile tile)
        {
            Tile = tile;
        }

        public class PropertiesCls
        {
            public PropertiesCls(StatsCls stats)
            {
                
            }
        }

        public class StatsCls
        {
            public string ModelName;
            public string Name = "BasicStructure";
            public string VisibleName = "NOVISIBLENAME";

            public bool Directional = false;

            public void Inherit(StatsCls original)
            {
                ModelName = original.ModelName;
                VisibleName = original.VisibleName;
                Directional = original.Directional;
            }
        }
    }
}
