﻿using System.Collections.Generic;
using Assets.Scripts.Game.Logic;
using Assets.Scripts.Game.Map;

namespace Assets.Scripts.Game.Entities
{
    public class Unit
    {
        public Unit(StatsCls stats)
        {
            Stats = stats;
            Properties = new PropertiesCls(stats);
            OrdersList = new List<Order>();
            Statuses = new List<Status>();
            Facing = MapTile.Direction.South;
        }

        public List<Order> OrdersList { get; set; }
        public Order CurrentOrder { get; set; }

        public List<Status> Statuses { private set; get; }

        public MapTile.Direction Facing { get; set; }
        private MapTile currentTile;
        public MapTile CurrentTile
        {
            get { return currentTile; }
            private set
            {
                if (currentTile != null)
                    currentTile.Units.Remove(this);
                currentTile = value;
                if (currentTile != null)
                    currentTile.Units.Add(this);
            }
        }
        public PropertiesCls Properties { private set; get; }
        public StatsCls Stats { private set; get; }

        public void SetCurrentTile(MapTile tile)
        {
            CurrentTile = tile;
        }

        public bool Alive {
            get { return Properties.Strength > 0; }
        }

        public Faction Faction { get; set; }

        public class PropertiesCls
        {
            public PropertiesCls(StatsCls stats)
            {
                Moves = stats.MaxMoves;
                Strength = stats.MaxStrength;
                Organization = stats.MaxOrganization;

                UniqueName = stats.VisibleName;
            }

            public int Moves;
            public int Strength;
            public float Organization;

            public string UniqueName;
        }

        public class StatsCls
        {
            public string Name = "BasicUnit";
            public string ModelName = "BasicUnit";
            public string VisibleName = "NOVISIBLENAME";

            public int MaxMoves = 1;
            public int MaxStrength = 1000;
            public float MaxOrganization = 10f;
            public int Attack = 1;
            public int Defence = 0;

            public void Inherit(StatsCls original)
            {
                ModelName = original.ModelName;
                VisibleName = original.VisibleName;

                MaxMoves = original.MaxMoves;
                MaxStrength = original.MaxStrength;
                MaxOrganization = original.MaxOrganization;
                Attack = original.Attack;
                Defence = original.Defence;
            }
        }
    }

    public class Status
    {
        
    }
}
