﻿using System.Collections.Generic;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;

namespace Assets.Scripts.Game
{
    public class GameData
    {
        public Dictionary<string, Structure.StatsCls> StructureTypes { get; private set; }
        public Dictionary<string, Unit.StatsCls> UnitTypes { get; private set; }
        public Dictionary<string, Faction> Factions { get; private set; }
        public readonly Dictionary<OrderType, int> OrderPriorities = new Dictionary<OrderType, int>();

        public GameData(Dictionary<string, Structure.StatsCls> structureTypes, Dictionary<string, Unit.StatsCls> unitTypes, Dictionary<string, Faction> factions)
        {
            StructureTypes = structureTypes;
            UnitTypes = unitTypes;
            Factions = factions;

            OrderPriorities.Add(OrderType.Move, 2);
            OrderPriorities.Add(OrderType.Bombard, 4);
            OrderPriorities.Add(OrderType.Standby, 0);
        }
    }
}
