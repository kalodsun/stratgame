﻿
using Assets.Scripts.Game.Map;

namespace Assets.Scripts.Game.TiledMapGenerator
{
    interface IMapGenerator
    {
        void GenerateMap(TiledMap map);
    }
}
