﻿using System;
using System.IO;
using System.Linq;
using Assets.Scripts.Game.Map;
using UnityEngine;
using Random = System.Random;

//for Texture2D

namespace Assets.Scripts.Game.TiledMapGenerator
{
    class BasicGenerator : IMapGenerator
    {
        private readonly Random rand = new Random();

        private const float TEMPERATURE_JITTER = 1f;

        public BasicGenerator()
        {
        }

        private const float TEMPERATURE_BASE = 24.0f;
        private const float TEMPERATURE_NORTH_FACTOR = 0.0f;
        private const float TEMPERATURE_HEIGHT_FACTOR = -0.1f;

        public void GenerateMap(TiledMap map)
        {
            foreach (var tile in map.Tiles)
            {
                if(tile.Properties.Temperature < 0)
                    tile.Properties.Temperature = TEMPERATURE_BASE + 
                        tile.Y * TEMPERATURE_NORTH_FACTOR + 
                        (float)(rand.NextDouble() * TEMPERATURE_JITTER) + 
                        tile.Height * TEMPERATURE_HEIGHT_FACTOR;
                if (tile.Properties.Ruggedness < 0)
                    tile.Properties.Ruggedness = CalculateRuggedness(tile, map);

                if (tile.Properties.Forestation < 0)
                {
                    if (tile.Height > 0 && tile.Height + tile.Properties.Ruggedness > 15f)
                        tile.Properties.Forestation = Mathf.Clamp(tile.Properties.Ruggedness/60f, 0.0f, 1.0f);
                    else
                        tile.Properties.Forestation = 0;
                }

                if (tile.Properties.Humidity < 0)
                    tile.Properties.Humidity = 0;
            }

            map.RebuildPathfinding();
        }

        private static float CalculateRuggedness(MapTile tile, TiledMap map)
        {
            float ret = 0;

            int x = tile.X, y = tile.Y;

            foreach (IntVector2 mod in MapTile.NeighbourSet)
            {
                if (Utils.IsValidCoord(x + mod.x, map.Width) && Utils.IsValidCoord(y + mod.y, map.Height))
                    ret += Math.Abs(tile.Height - map.Tiles[x + mod.x, y + mod.y].Height);
            }

            return ret;
        }
    }
}
