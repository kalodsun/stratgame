﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;
using Assets.Scripts.Game.Map;

namespace Assets.Scripts.Game.IO
{
    class DataSaver
    {
        public static void SaveMap(Stream fileStream, TiledMap map, GameLogic gameLogic)
        {
            StreamWriter writer = new StreamWriter(fileStream);

            int waterLevel;
            string heightmapName;
            map.GetExtraData(out heightmapName, out waterLevel);

            writer.WriteLine("Heightmap " + heightmapName);
            writer.WriteLine("WaterLevel " + waterLevel);

            writer.WriteLine("Structures");
            writer.WriteLine("{");
            foreach (Structure structure in gameLogic.Structures)
            {
                writer.WriteLine("\t" + structure.Stats.Name + " " + structure.Tile.X + " " + structure.Tile.Y);
            }
            writer.WriteLine("}");

            writer.WriteLine("Units");
            writer.WriteLine("{");
            foreach (Unit unit in gameLogic.Units)
            {
                writer.WriteLine("\t" + unit.Stats.Name + " " + unit.CurrentTile.X + " " + unit.CurrentTile.Y + " " + unit.Faction.Name);
            }
            writer.WriteLine("}");

            writer.WriteLine("Properties");
            writer.WriteLine("{");
            for (int x = 0; x < map.Width; x++)
            {
                for (int y = 0; y < map.Height; y++)
                {
                    MapTile tile = map.Tiles[x, y];
                    writer.WriteLine("\t" + x + " " + y + " " + tile.Properties.Temperature + " " + tile.Properties.Ruggedness + " " + tile.Properties.Forestation + " " + tile.Properties.Humidity);
                }
            }
            writer.WriteLine("}");

            foreach (River river in map.Rivers)
            {
                writer.WriteLine("River");
                writer.WriteLine("{");
                for (int i = 0; i < river.RiverNodes.Count; i++)
                    writer.WriteLine("\t" + river.RiverNodes[i].x + " " + river.RiverNodes[i].y);
                writer.WriteLine("}");
            }

            writer.Flush();
            writer.Close();
        }

        public static void SaveMap(string mapDataFile, TiledMap map, GameLogic gameLogic)
        {
            if(File.Exists(mapDataFile))
                File.Delete(mapDataFile);
            
            SaveMap(File.OpenWrite(mapDataFile), map, gameLogic);
        }
    }
}
