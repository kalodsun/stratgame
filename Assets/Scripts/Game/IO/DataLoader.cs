﻿using System;
using System.Collections.Generic;
using System.IO;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;
using Assets.Scripts.Game.Map;
using Assets.Scripts.Game.TiledMapGenerator;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Game.IO
{
    static class DataLoader
    {
        private const string HEIGHTMAP_PATH = "Data/Textures/Heightmaps/";

        public static TiledMap LoadMap(GameLogic gameLogic, string mapDataFile, IMapGenerator mapGenerator = null)
        {
            return LoadMap(gameLogic, File.OpenRead(mapDataFile), mapGenerator);
        } 

        public static TiledMap LoadMap(GameLogic gameLogic, Stream stream, IMapGenerator mapGenerator = null)
        {
            Texture2D heightmap;
            int waterLevel;
            string heightmapName;

            LoadHeightmap(stream, out heightmap, out waterLevel, out heightmapName);
            stream.Position = 0;

            TiledMap map = new TiledMap(heightmap.width, heightmap.height);
            map.SetExtraData(heightmapName, waterLevel);

            for (int h = 0; h < heightmap.height; h++)
                for (int w = 0; w < heightmap.width; w++)
                {
                    map.Tiles[w, h] = new MapTile
                    {
                        Height = (int)(heightmap.GetPixel(w, h).grayscale * 256f) - waterLevel,
                        X = w,
                        Y = h
                    };

                    if (map.MaxTileHeight < map.Tiles[w, h].Height)
                        map.MaxTileHeight = map.Tiles[w, h].Height;
                }

            LoadMapData(gameLogic, map, stream);

            if (mapGenerator != null)
                mapGenerator.GenerateMap(map);

            stream.Close();

            map.RebuildPathfinding();
            gameLogic.SetMap(map);
            return map;
        }

        public static Dictionary<string, Faction> LoadFactions(string factionsDataFile)
        {
            Dictionary<string, Faction> factions = new Dictionary<string, Faction>();

            var reader = new StringReader(File.ReadAllText(factionsDataFile));

            Faction currentFaction = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    factions[currentFaction.Name] = currentFaction;
                    currentFaction = null;
                    continue;
                }

                if (words[0] == "{")
                    continue;

                if (currentFaction != null)
                {
                    switch (words[0])
                    {
                        case "VisibleName":
                            string name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i] + " ";
                            currentFaction.VisibleName = name;
                            break;
                        case "Parent":
                            //todo
                            break;
                        case "Color":
                            currentFaction.Color = Convert.ToUInt32(words[1], 16);
                            break;
                        default:
                            Debug.LogError("Invalid entry in faction data for " + currentFaction.Name + ": " + words[0]);
                            break;
                    }
                }
                else
                {
                    if (words[0] != "Relations")
                    {
                        currentFaction = new Faction();
                        currentFaction.Name = words[0];
                    }
                    else
                    {
                        ReadFactionRelations(reader, factions);
                    }
                }
            }

            return factions;
        }

        private static void ReadFactionRelations(StringReader reader, Dictionary<string, Faction> factions)
        {
            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    return;
                }

                if (words[0] == "{")
                {
                    continue;
                }

                if (!factions.ContainsKey(words[0]) || !factions.ContainsKey(words[1]))
                {
                    Debug.LogError("Invalid entry in faction relation data for " + words[0] + " / " + words[1]);
                    continue;
                }

                Faction.Relation relation = (Faction.Relation) Enum.Parse(typeof (Faction.Relation), words[2]);

                factions[words[0]].Relations.Add(factions[words[1]], relation);
                factions[words[1]].Relations.Add(factions[words[0]], relation);
            }
        }

        public static Dictionary<string, Structure.StatsCls> LoadStructures(string structureDataFile)
        {
            Dictionary<string, Structure.StatsCls> structures = new Dictionary<string, Structure.StatsCls>();

            var reader = new StringReader(File.ReadAllText(structureDataFile));

            Structure.StatsCls currentStructure = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    structures[currentStructure.Name] = currentStructure;
                    currentStructure = null;
                    continue;
                }

                if (words[0] == "{")
                    continue;

                if (currentStructure != null)
                {
                    string name;
                    switch (words[0])
                    {
                        case "InheritFrom":
                            if (!structures.ContainsKey(words[1]))
                                Debug.LogError("Invalid structure to inherit from in: " + currentStructure.Name + ": " + words[1]);

                            currentStructure.Inherit(structures[words[1]]);
                            break;
                        case "VisibleName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i] + " ";
                            currentStructure.VisibleName = name;
                            break;
                        case "ModelName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i];
                            currentStructure.ModelName = name;
                            break;
                        case "Directional":
                            if (words[1] == "yes")
                                currentStructure.Directional = true;
                            else if (words[1] == "no")
                                currentStructure.Directional = false;
                            else
                                Debug.LogError("Invalid value for structure - isDirectional in " + currentStructure.Name + ": " + words[1]);
                            break;
                        default:
                            Debug.LogError("Invalid entry in structure data for " + currentStructure.Name + ": " + words[0]);
                            break;
                    }
                }
                else
                {
                    currentStructure = new Structure.StatsCls();
                    currentStructure.Name = words[0];
                }
            }

            return structures;
        }

        public static Dictionary<string, Unit.StatsCls> LoadUnits(string unitDataFile)
        {
            Dictionary<string, Unit.StatsCls> units = new Dictionary<string, Unit.StatsCls>();

            var reader = new StringReader(File.ReadAllText(unitDataFile));

            Unit.StatsCls currentUnit = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    units[currentUnit.Name] = currentUnit;
                    currentUnit = null;
                    continue;
                }

                if (words[0] == "{")
                    continue;

                if (currentUnit != null)
                {
                    string name;
                    switch (words[0])
                    {
                        case "InheritFrom":
                            if (!units.ContainsKey(words[1]))
                                Debug.LogError("Invalid structure to inherit from in: " + currentUnit.Name + ": " + words[1]);

                            currentUnit.Inherit(units[words[1]]);
                            break;
                        case "VisibleName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i] + " ";
                            currentUnit.VisibleName = name;
                            break;
                        case "ModelName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i];
                            currentUnit.ModelName = name;
                            break;
                        case "Organization":
                            currentUnit.MaxOrganization = float.Parse(words[1]);
                            break;
                        case "Strength":
                            currentUnit.MaxStrength= int.Parse(words[1]);
                            break;
                        case "Moves":
                            currentUnit.MaxMoves = int.Parse(words[1]);
                            break;
                        case "Attack":
                            currentUnit.Attack = int.Parse(words[1]);
                            break;
                        case "Defence":
                            currentUnit.Defence = int.Parse(words[1]);
                            break;
                        default:
                            Debug.LogError("Invalid entry in unit data for " + currentUnit.Name + ": " + words[0]);
                            break;
                    }
                }
                else
                {
                    currentUnit = new Unit.StatsCls();
                    currentUnit.Name = words[0];
                }
            }

            return units;
        }

        private static void LoadHeightmap(Stream stream, out Texture2D heightmap, out int heightmapWaterLevel, out string heightmapName)
        {
            var reader = new StreamReader(stream);

            heightmap = null;
            heightmapWaterLevel = 0;
            heightmapName = "";

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                switch (words[0].ToLower())
                {
                    case "heightmap":
                        heightmap = new Texture2D(1, 1);
                        heightmap.LoadImage(File.ReadAllBytes(HEIGHTMAP_PATH + words[1]));
                        heightmapName = words[1];
                        break;
                    case "waterlevel":
                        heightmapWaterLevel = int.Parse(words[1]);
                        break;
                }
            }
        }

        private enum MapDataBlock
        {
            Properties, Structures, Units, River, None
        }

        private static void LoadMapData(GameLogic gameLogic, TiledMap map, Stream stream)
        { 
            var reader = new StreamReader(stream);

            var currentBlock = MapDataBlock.None;
            string currentObject = null;

            MapTile curTile;
            River curRiver = null;
            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();
                curTile = null;

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    if (curRiver != null)
                    {
                        map.AddRiver(curRiver);
                        curRiver = null;
                    }

                    if (currentObject != null)
                    {
                        currentObject = null;

                        if(currentBlock == MapDataBlock.River)
                            currentBlock = MapDataBlock.None;
                    }
                    else
                        currentBlock = MapDataBlock.None;

                    continue;
                }

                if(words[0] == "{")
                    continue;

                if (currentBlock == MapDataBlock.None)
                {
                    switch (words[0].ToLower())
                    {
                        case "structures":
                            currentBlock = MapDataBlock.Structures;
                            break;
                        case "units":
                            currentBlock = MapDataBlock.Units;
                            break;
                        case "properties":
                            currentBlock = MapDataBlock.Properties;
                            break;
                        case "river":
                            currentBlock = MapDataBlock.River;
                            currentObject = "A river";
                            curRiver = new River();
                            break;
                        case "heightmap":
                        case "waterlevel":
                            break;
                        default:
                            Debug.LogError("Invalid block in map data: " + words[0]);
                            break;
                    }
                }
                else
                {
                    if (currentObject == null)
                    {
                        switch (currentBlock)
                        {
                            case MapDataBlock.Properties:
                                curTile = map.Tiles[int.Parse(words[0]), int.Parse(words[1])];

                                if(words[2] != "U")
                                    curTile.Properties.Temperature = float.Parse(words[2]);
                                if (words[3] != "U")
                                    curTile.Properties.Ruggedness = float.Parse(words[3]);
                                if (words[4] != "U")
                                    curTile.Properties.Forestation = float.Parse(words[4]);
                                if (words[5] != "U")
                                    curTile.Properties.Humidity = float.Parse(words[5]);
                                break;
                            case MapDataBlock.Units:
                                if (words.Length == 1)
                                    currentObject = words[0];
                                else
                                {
                                    curTile = map.Tiles[int.Parse(words[1]), int.Parse(words[2])];
                                    gameLogic.CreateUnit(gameLogic.gameData.UnitTypes[words[0]], curTile, gameLogic.gameData.Factions[words[3]]);
                                }
                                break;
                            case MapDataBlock.Structures:
                                if (words.Length == 1)
                                    currentObject = words[0];
                                else
                                {
                                    curTile = map.Tiles[int.Parse(words[1]), int.Parse(words[2])];

                                    Structure.StatsCls structureType = gameLogic.gameData.StructureTypes[words[0]];

                                    MapTile.Direction direction = 0;
                                    if (structureType.Directional)
                                        direction = (MapTile.Direction) Enum.Parse(typeof(MapTile.Direction), words[3]);

                                    gameLogic.CreateStructure(structureType, curTile, direction);
                                }
                                break;
                            case MapDataBlock.River:
                                throw new Exception("Invalid reader state: inside river block, but no currentObject");
                        }
                    }
                    else
                    {  
                        switch (currentBlock)
                        {
                            case MapDataBlock.Structures:
                                curTile = map.Tiles[int.Parse(words[0]), int.Parse(words[1])];

                                Structure.StatsCls structureType = gameLogic.gameData.StructureTypes[currentObject];

                                MapTile.Direction direction = 0;
                                if (structureType.Directional)
                                    direction = (MapTile.Direction)Enum.Parse(typeof(MapTile.Direction), words[2]);

                                gameLogic.CreateStructure(structureType, curTile, direction);
                                break;
                            case MapDataBlock.Units:
                                curTile = map.Tiles[int.Parse(words[0]), int.Parse(words[1])];
                                gameLogic.CreateUnit(gameLogic.gameData.UnitTypes[currentObject], curTile, gameLogic.gameData.Factions[words[2]]);
                                break;
                            case MapDataBlock.River:
                                curRiver.RiverNodes.Add(new IntVector2(Convert.ToInt32(words[0]), Convert.ToInt32(words[1])));//TODO adjesency check
                                break;
                        }
                    }
                }
            }
        }
    }
}
