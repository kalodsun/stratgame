﻿using UnityEngine;

namespace Assets.Scripts.Game.Map
{
    public static class MapUtils
    {
        public static MapTile.Direction GetFacingDirection(IntVector2 position, IntVector2 target)
        {
            Vector2 diffVector = target - position;

            bool north = Vector3.Cross(new Vector3(-1, 0, 0), new Vector3(diffVector.x, diffVector.y)).z < 0;
            int angle = (int) Vector2.Angle(Vector2.left, diffVector);

            if (north)
                angle += 2*(180 - angle);

            angle += 23;
            angle %= 360;

            return (MapTile.Direction) (angle/45);
        }
    }
}
