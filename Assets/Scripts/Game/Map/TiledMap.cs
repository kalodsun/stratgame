﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.TiledMapPathfinder;
using UnityEngine;

namespace Assets.Scripts.Game.Map
{
    public enum BorderType { Land, River }

    public class TiledMap {
        private IMapPathfinder pathfinder;

        private string heightmapName;
        private int waterLevel;

        private static bool checkRoad(MapTile from, MapTile to)
        {
            foreach (Structure structure in from.Structures)
            {
                if (structure.Direction == Utils.GetDirection(to.X - from.X, to.Y - from.Y))
                    if (structure.Stats.Name == "Road")
                        return true;
            }

            return false;
        }

        private readonly AStarPathfinder.EdgeWeightDelegate heightDifferenceWeight = delegate(MapTile from, MapTile to, TiledMap map)
        {
            if (from.Height < 0 || to.Height < 0 || map.GetBorderType(from, to) == BorderType.River)
                return -1f;

            float dx = from.X - to.X;
            float dy = from.Y - to.Y;
            float dist = Mathf.Sqrt(dx * dx + dy * dy) + Mathf.Abs(from.Height - to.Height)*5;
            dist *= 5;

            if (checkRoad(from, to))
                dist /= 5;

            return  dist;
        };

        private readonly AStarPathfinder.EdgeWeightDelegate pureDistanceWeight = delegate (MapTile from, MapTile to, TiledMap map)
        {
            if (from.Height < 0 || to.Height < 0 || map.GetBorderType(from, to) == BorderType.River)
                return -1f;

            float dx = from.X - to.X;
            float dy = from.Y - to.Y;
            float dist = Mathf.Sqrt(dx*dx + dy*dy);
            dist *= 5;

            if (checkRoad(from, to))
                dist /= 5;

            return dist;
        };

        public MapTile[,] Tiles { get; private set; }

        //BottomLeft edges of the coordinate
        public BorderType[,] HorizontalEdges { get; private set; }
        public BorderType[,] VerticalEdges { get; private set; }

        public List<River> Rivers { get; private set; }

        public int Width { get; private set; }

        public int Height { get; private set; }

        public int MaxTileHeight { get; set; }

        public TiledMap(int width, int height)
        {
            Tiles = new MapTile[width, height];
            HorizontalEdges = new BorderType[width+1, height+1];
            VerticalEdges = new BorderType[width+1, height+1];
            Rivers = new List<River>();
            Width = width;
            Height = height;
        }

        public void SetExtraData(string heightmapName, int waterLevel)
        {
            this.heightmapName = heightmapName;
            this.waterLevel = waterLevel;
        }

        public void GetExtraData(out string heightmapName, out int waterLevel)
        {
            heightmapName = this.heightmapName;
            waterLevel = this.waterLevel;
        }

        public void RebuildPathfinding()
        {
            pathfinder = new AStarPathfinder(this, pureDistanceWeight);
        }

        public List<MapTile> FindPath(MapTile start, MapTile end)
        {
            return pathfinder.FindPath(start, end);
        }

        public MapTile GetTile(IntVector2 coords)
        {
            return Tiles[coords.x, coords.y];
        }

        public MapTile GetNeighbourTile(MapTile tile, MapTile.Direction direction)
        {
            IntVector2 coords = tile.GetPosition();
            coords += MapTile.NeighbourSet[(int) direction];

            if (Utils.IsValidCoord(coords.x, Width) && Utils.IsValidCoord(coords.y, Height))
                return Tiles[coords.x, coords.y];

            return null;
        }

        public bool IsNeigbour(MapTile a, MapTile b)
        {
            int dx, dy;
            dx = b.X - a.X;
            dy = b.Y - a.Y;

            if (dx > 1 || dx < -1 || dy > 1 || dy < -1 || a == b)
                return false;

            return true;
        }

        public BorderType GetOrthogonalBorderType(MapTile tile, MapTile.Direction direction)
        {
            switch (direction)
            {
                case MapTile.Direction.West:
                    return VerticalEdges[tile.X, tile.Y];
                case MapTile.Direction.South:
                    return HorizontalEdges[tile.X, tile.Y];
                case MapTile.Direction.East:
                    return VerticalEdges[tile.X + 1, tile.Y];
                case MapTile.Direction.North:
                    return HorizontalEdges[tile.X, tile.Y + 1];
                default:
                    throw new ArgumentException("Direction is not orthogonal");
            }
        }

        public BorderType GetBorderType(MapTile a, MapTile b)
        {
            int dx, dy;
            dx = b.X - a.X;
            dy = b.Y - a.Y;

            if (dx > 1 || dx < -1 || dy > 1 || dy < -1 || a == b)
                throw new ArgumentException("Given Tiles are not adjascent!");

            if (dx == 0 || dy == 0)
                return GetOrthogonalBorderType(a, Utils.GetDirection(dx, dy));

            if(GetOrthogonalBorderType(a, Utils.GetDirection(dx, 0)) == BorderType.Land && GetOrthogonalBorderType(b, Utils.GetDirection(0, -dy)) == BorderType.Land)
                return BorderType.Land;

            if (GetOrthogonalBorderType(a, Utils.GetDirection(0, dy)) == BorderType.Land && GetOrthogonalBorderType(b, Utils.GetDirection(-dx, 0)) == BorderType.Land)
                return BorderType.Land;

            return BorderType.River;
        }

        public void AddRiver(River river)
        {
            Rivers.Add(river);

            ProcessRiver(river);
        }

        public void RemoveRiver(River river)
        {
            if(!Rivers.Contains(river))
                throw new ArgumentException("River to be removed hasn't been added");

            Rivers.Remove(river);
            UpdateAllRivers();
        }

        public void UpdateAllRivers()
        {
            for(int i = 0;i < Width+1;i++)
                for (int j = 0; j < Height + 1; j++)
                {
                    HorizontalEdges[i, j] = BorderType.Land;
                    VerticalEdges[i, j] = BorderType.Land;
                }

            foreach (River river in Rivers)
            {
                ProcessRiver(river);
            }
        }

        private void ProcessRiver(River river)
        {
            List<IntVector2> nodes = river.RiverNodes;
            for (int i = 1; i < river.RiverNodes.Count; i++)
            {
                int dx, dy;
                dx = nodes[i].x - nodes[i - 1].x;
                dy = nodes[i].y - nodes[i - 1].y;

                if (dx == 0)
                {
                    if (dy == -1)
                        VerticalEdges[nodes[i].x, nodes[i].y] = BorderType.River;
                    else
                        VerticalEdges[nodes[i - 1].x, nodes[i - 1].y] = BorderType.River;
                }
                else if (dy == 0)
                {
                    if (dx == -1)
                        HorizontalEdges[nodes[i].x, nodes[i].y] = BorderType.River;
                    else
                        HorizontalEdges[nodes[i - 1].x, nodes[i - 1].y] = BorderType.River;
                }
                else
                    throw new ArgumentException("River Tiles are not orthogonally adjascent! (" + nodes[i - 1].x + "," + nodes[i - 1].y + ") and (" + nodes[i].x + "," + nodes[i].y + ")");
            }
        }
    }
}
