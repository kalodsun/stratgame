﻿using System.Collections.Generic;
using Assets.Scripts.Game.Entities;

namespace Assets.Scripts.Game.Map
{
    public class MapTile {
        public static readonly IntVector2[] NeighbourSet = { new IntVector2(-1, 0), new IntVector2(-1, -1), new IntVector2(0, -1), new IntVector2(1, -1), new IntVector2(1, 0), new IntVector2(1, 1), new IntVector2(0, 1), new IntVector2(-1, 1) };
        public enum Direction
        {
            West = 0, Southwest, South, Southeast, East, Northeast, North, Northwest
        }

        public int TileSize { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int Height { get; set; }

        public PropertiesCls Properties { get; set; }

        public List<Unit> Units { get; set; } 
        public List<Structure> Structures { get; set; } 

        public MapTile()
        {
            X = Y = Height = 0;

            Properties = new PropertiesCls();
            Units = new List<Unit>();
            Structures = new List<Structure>();
        }

        public string GetTilePosString()
        {
            return "(" + X + "," + Y + ")";
        }

        public IntVector2 GetPosition()
        {
            return new IntVector2(X, Y);
        }

        public class PropertiesCls
        {
            public float Ruggedness = -1f, Humidity = -1f, Temperature = -1f, Forestation = -1f;
        }
    }
}