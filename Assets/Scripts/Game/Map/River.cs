﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Game.Map
{
    public class River
    {
        //BottomLeft node of x,y tile
        public List<IntVector2> RiverNodes { get; private set; }

        public River()
        {
            RiverNodes = new List<IntVector2>();
        }
    }
}
