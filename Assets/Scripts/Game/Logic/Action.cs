﻿using System.Collections.Generic;

namespace Assets.Scripts.Game.Logic
{
    public class Action : Order
    {
        public int Priority { get; set; }
        public bool Executed { get; set; }
    }

    class ActionComparer : Comparer<Action>
    {
        public override int Compare(Action x, Action y)
        {
            if (x.Priority > y.Priority)
                return -1;
            if (x.Priority == y.Priority)
                return 0;
            return 1;
        }
    }
}
