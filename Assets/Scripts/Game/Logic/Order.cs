﻿using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Map;

namespace Assets.Scripts.Game.Logic
{
    public class Order
    {
        public OrderType Type { get; set; }

        public MapTile Target { get; set; }
        public Unit Source { get; set; }
    }

    public enum OrderType
    {
        Move, Bombard, Standby
    }
}
