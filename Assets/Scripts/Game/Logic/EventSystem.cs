﻿
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Map;

namespace Assets.Scripts.Game.Logic
{
    public static class EventSystem
    {
        public enum UnitEventType { Created, GivenOrders, Moved, Damaged, Destroyed }
        public enum TileEventType { Changed, UnitEntered, UnitLeft }

        public delegate void UnitEventDelegate(UnitEventType eventType, Unit unit, Unit extraUnit = null);
        public delegate void TileEventDelegate(TileEventType eventType, MapTile tile);

        public static event UnitEventDelegate UnitEvent;
        public static event TileEventDelegate TileEvent;

        public static void SendUnitEvent(UnitEventType eventType, Unit unit, Unit extraUnit = null)
        {
            if(UnitEvent != null)
                UnitEvent.Invoke(eventType, unit, extraUnit);
        }

        public static void SendTileEvent(TileEventType eventType, MapTile tile)
        {
            if (TileEvent != null)
                TileEvent.Invoke(eventType, tile);
        }

    }
}
