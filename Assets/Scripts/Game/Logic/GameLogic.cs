﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Map;
using UnityEngine;

// ReSharper disable UseNameofExpression

namespace Assets.Scripts.Game.Logic
{
    public class GameLogic
    {
        private TiledMap map;

        private int turnNumber = 0;

        public GameData gameData;
     
        public List<Structure> Structures { get; private set; }
        public List<Unit> Units { get; private set; } 

        private readonly List<Action> actions = new List<Action>();   

        public GameLogic(GameData gameData)
        {
            this.gameData = gameData;

            Units = new List<Unit>();
            Structures = new List<Structure>();
        }

//        public void ChangeMap(TiledMap map)
//        {
//            this.map = map;
//            turnNumber = 0;
//            Units.Clear();
//            map.RebuildPathfinding();
//        }

        public void SetMap(TiledMap map)
        {
            if(this.map != null)
                Debug.LogError("GameLogic.SetMap called on non-empty gameLogic");
            this.map = map;
        }

        public int TurnNumber
        {
            get { return turnNumber; }
        }

        public Unit CreateUnit(Unit.StatsCls stats, MapTile tile, Faction faction)
        {
            Unit newUnit = new Unit(stats);
            newUnit.Faction = faction;
            newUnit.SetCurrentTile(tile);
            Units.Add(newUnit);

            EventSystem.SendUnitEvent(EventSystem.UnitEventType.Created, newUnit);

            return newUnit;
        }

        public void DestroyUnit(Unit unit)
        {
            unit.SetCurrentTile(null);
            unit.Properties.Strength = -1;
        }

        public Structure CreateStructure(Structure.StatsCls stats, MapTile tile, MapTile.Direction direction = 0)
        {
            Structure newStructure = new Structure(stats);
            newStructure.Direction = direction;
            newStructure.SetTile(tile);
            Structures.Add(newStructure);

            EventSystem.SendTileEvent(EventSystem.TileEventType.Changed, tile);

            return newStructure;
        }

        public void ProcessTurn()
        {
            while (true)
            {
                ProcessOrders();

                if(actions.Count == 0)
                    break;

                actions.Sort(new ActionComparer());

                //clears actions on end, so no inf. loop
                ExecuteActions();

                UpdateOrders();
            }

            ApplyEndturnEffects();

            turnNumber++;
        }

        //Fills up actions list with at most one action per unit. Does checks related to the executing unit to ensure that this is a valid action.
        private void ProcessOrders()
        {
            foreach (var unit in Units)
            {
                if(!unit.Alive || unit.Properties.Moves < 1)
                    continue;

                if (unit.CurrentOrder == null)
                {
                    if (unit.OrdersList.Count > 0)
                    {
                        unit.CurrentOrder = unit.OrdersList[0];
                        unit.OrdersList.RemoveAt(0);
                    }
                    else
                        continue;
                }

                Order order = unit.CurrentOrder;

                switch (order.Type)
                {
                    case OrderType.Move:
                        List<MapTile> path = map.FindPath(order.Source.CurrentTile, order.Target);

                        if (path.Count > 1)
                        {
                            actions.Add(new Action
                            {
                                Executed = false,
                                Priority = gameData.OrderPriorities[order.Type],
                                Source = order.Source,
                                Target = path[1],
                                Type = order.Type
                            });
                        }
                        else
                        {
                            unit.CurrentOrder = null;
                        }
                        break;
                    case OrderType.Bombard:
                        actions.Add(new Action
                        {
                            Executed = false,
                            Priority = gameData.OrderPriorities[order.Type],
                            Source = order.Source,
                            Target = order.Target,
                            Type = order.Type
                        });
                        break;
                    case OrderType.Standby:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void ExecuteActions()
        {
            for (int stage = 1; stage <= 3; stage++)
            {
                for (int i = 0; i < actions.Count; i++)
                {
                    if(actions[i].Executed || actions[i].Source.CurrentTile == null)
                        continue;

                    switch (actions[i].Type)
                    {
                        case OrderType.Move:
                            ExecuteMoveAction(actions[i], stage);
                            break;
                        case OrderType.Bombard:
                            break;
                        case OrderType.Standby:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            actions.Clear();
        }

        private void UpdateOrders()
        {
            foreach (Unit unit in Units)
            {
                if (unit.CurrentOrder != null)
                {
                    switch (unit.CurrentOrder.Type)
                    {
                        case OrderType.Move:
                            if (unit.CurrentTile == unit.CurrentOrder.Target)
                                unit.CurrentOrder = null;
                            break;
                        case OrderType.Bombard:
                        case OrderType.Standby:
                            unit.CurrentOrder = null;
                            break;
                    }
                }
            }
        }

        private void ApplyEndturnEffects()
        {
            foreach (var unit in Units)
            {
                unit.Properties.Moves = unit.Stats.MaxMoves;
                unit.Properties.Organization += unit.Stats.MaxOrganization/20;

                if (unit.Properties.Organization > unit.Stats.MaxOrganization)
                    unit.Properties.Organization = unit.Stats.MaxOrganization;
            }
        }

        private void ExecuteMoveAction(Action action, int stage)
        {
            switch (stage)
            {
                case 1:
                    //add moving/attacking modifier when they are implemented
                    break;
                case 2:
                    Unit unit = action.Source;
                    if (action.Target.Units.Count == 0)
                    {
                        EventSystem.SendTileEvent(EventSystem.TileEventType.UnitLeft, action.Source.CurrentTile);

                        unit.Facing = MapUtils.GetFacingDirection(unit.CurrentTile.GetPosition(),
                            action.Target.GetPosition());
                        unit.SetCurrentTile(action.Target);
                        action.Executed = true;

                        EventSystem.SendTileEvent(EventSystem.TileEventType.UnitEntered, action.Source.CurrentTile);
                        EventSystem.SendUnitEvent(EventSystem.UnitEventType.Moved, action.Source);
                    }
                    else
                    {
                        Faction otherFaction = action.Target.Units[0].Faction;
                        if (otherFaction != unit.Faction && (!unit.Faction.Relations.ContainsKey(otherFaction) || unit.Faction.Relations[otherFaction] == Faction.Relation.Enemy))
                            ExecuteAttack(action.Source, action.Target.Units[0]);
                    }
                    action.Source.Properties.Moves--;
                    break;
                case 3:
                    //clear modifiers
                    action.Executed = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("stage");
            }
        }

        private void ExecuteAttack(Unit attacker, Unit defender)
        {
            float defenceBonus = (float)(defender.CurrentTile.Height - attacker.CurrentTile.Height) / map.MaxTileHeight;

            float aStrFract = ((float) attacker.Properties.Strength/attacker.Stats.MaxStrength);
            float dStrFract = ((float) defender.Properties.Strength/defender.Stats.MaxStrength);

            float aOrgFract = ((float)attacker.Properties.Organization / attacker.Stats.MaxOrganization);
            float dOrgFract = ((float)defender.Properties.Organization / defender.Stats.MaxOrganization);

            defender.Properties.Strength -= (int)(attacker.Stats.Attack * aStrFract * aOrgFract * (1.4f - dOrgFract) * (1f - defenceBonus));

            attacker.Properties.Strength -= (int)(defender.Stats.Attack * dStrFract * dOrgFract * (1.5f - aOrgFract) * (1f + 2 * defenceBonus));

            attacker.Properties.Organization -= 1.5f;
            defender.Properties.Organization -= 1;

            if (defender.Properties.Organization < 0)
                defender.Properties.Organization = 0;

            if (attacker.Properties.Organization < 0)
                attacker.Properties.Organization = 0;

            EventSystem.SendUnitEvent(EventSystem.UnitEventType.Damaged, attacker);
            EventSystem.SendUnitEvent(EventSystem.UnitEventType.Damaged, defender);

            if (attacker.Properties.Strength < 0)
                DestroyUnit(attacker);

            if (defender.Properties.Strength < 0)
                DestroyUnit(defender);
        }
    }
}
