﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.IO;
using Assets.Scripts.Game.Logic;
using Assets.Scripts.Game.Map;
using Assets.Scripts.Game.TiledMapGenerator;
using Assets.Scripts.Graphics;
using UnityEngine;
using UnityEngine.UI;
using Application = UnityEngine.Application;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once UnassignedField.Global
    public class MainScript : MonoBehaviour
    {

        public Text InfoText, HelpText, FpsText, MessageText;

        public GameObject TerrainElement, RiverElement;
        public float CameraMoveSpeed = 0.13f, CameraRotationSpeed = 1f;
        public float TileSize, HeightScale;
        public string MapData, UnitData, StructureData, FactionData;

        private Unit selectedUnit = null;
        private TiledMap tiledMap;
        private GameGraphics graphics;
        private IMapGenerator mapGenerator;
        private GameLogic gameLogic;
        private GameData gameData;

        enum EditorMode
        {
            None, River
        }

        private EditorMode currentMode;

        // ReSharper disable once UnusedMember.Local
        void Start () 
        {
            currentMode = EditorMode.None;

            Dictionary<string, Unit.StatsCls> unitTypes = DataLoader.LoadUnits(UnitData);
            Dictionary<string, Structure.StatsCls> structureTypes = DataLoader.LoadStructures(StructureData);
            Dictionary<string, Faction> factions = DataLoader.LoadFactions(FactionData);

            gameData = new GameData(structureTypes, unitTypes, factions);

            gameLogic = new GameLogic(gameData);
            tiledMap = DataLoader.LoadMap(gameLogic, MapData, new BasicGenerator());

            graphics = new GameGraphics(tiledMap, gameLogic, TerrainElement, RiverElement, TileSize, HeightScale);
            graphics.GenerateTileGraphics(tiledMap.Width, tiledMap.Height);
            graphics.AssembleTiles();
            graphics.RedrawRivers();
            graphics.UpdateAllUnits();

            EventSystem.UnitEvent += (type, unit, extraUnit) =>
            {
                switch (type)
                {
                    case EventSystem.UnitEventType.Created:
                    case EventSystem.UnitEventType.Moved:
                    case EventSystem.UnitEventType.Damaged:
                    case EventSystem.UnitEventType.GivenOrders:
                        graphics.UpdateUnit(unit);
                        if(unit == selectedUnit)
                            UpdateSelectionInterface();
                        break;
                    case EventSystem.UnitEventType.Destroyed:
                        graphics.RemoveUnit(unit);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("type", type, null);
                }
            };
        }

        private float dt = 0;
        private int frames = 0;

        // ReSharper disable once UnusedMember.Local
        void Update () 
        {
            HandleMouseover();
            HandleMouse();
            HandleCamera();

            if (Input.GetKeyDown(KeyCode.M))
            {
                SaveFileDialog dialog = new SaveFileDialog
                {
                    FileName = "SavedMap",
                    DefaultExt = "txt",
                    Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*",
                    InitialDirectory = "Data/Map/"
                };
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    DataSaver.SaveMap(dialog.OpenFile(), tiledMap, gameLogic);
                    MessageText.text = "Map saved";
                }
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                OpenFileDialog dialog = new OpenFileDialog()
                {
                    DefaultExt = "txt",
                    Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*",
                    InitialDirectory = "Data/Map/"
                };
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    gameLogic = new GameLogic(gameData);
                    tiledMap = DataLoader.LoadMap(gameLogic, dialog.OpenFile());
                    graphics.ChangeMap(gameLogic, tiledMap);
                    MessageText.text = "Map changed";
                }
            }

            if (Input.GetKeyDown(KeyCode.G))
            {
                new BasicGenerator().GenerateMap(tiledMap);

                graphics.UpdateAllTiles();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                if(currentMode == EditorMode.River)
                    currentMode = EditorMode.None;
                else
                    currentMode = EditorMode.River;
            }

            frames++;
            dt += Time.deltaTime;
            if (dt > 0.25f)
            {
                FpsText.text = " FPS:" + (frames*4);
                dt = 0;
                frames = 0;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (currentMode == EditorMode.None)
                {
                    gameLogic.ProcessTurn();

                    graphics.UpdateAllUnits();
                }
                else if(currentMode == EditorMode.River)
                {
                    currentRiver = null;
                    currentMode = EditorMode.None;
                }
            }

            if (Input.GetKeyDown(KeyCode.F1))
            {
                HelpText.gameObject.SetActive(!HelpText.gameObject.activeSelf);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        private void HandleCamera()
        {
            Vector3 movement = Vector3.zero;
            Quaternion rotation = Quaternion.identity;

            float effectiveMoveSpeed = Input.GetKey(KeyCode.LeftShift) ? CameraMoveSpeed * 5 : CameraMoveSpeed;
            float effectiveRotationSpeed = Input.GetKey(KeyCode.LeftShift) ? CameraRotationSpeed * 5 : CameraRotationSpeed;

            if (Input.GetKey(KeyCode.W))
                movement += new Vector3(0, 0, effectiveMoveSpeed);
            if (Input.GetKey(KeyCode.A))
                movement += new Vector3(-effectiveMoveSpeed, 0, 0);
            if (Input.GetKey(KeyCode.S))
                movement += new Vector3(0, 0, -effectiveMoveSpeed);
            if (Input.GetKey(KeyCode.D))
                movement += new Vector3(effectiveMoveSpeed, 0, 0);

            Camera.main.transform.position += Quaternion.Euler(0, Camera.main.transform.rotation.eulerAngles.y, 0) * movement;

            if (Input.GetKey(KeyCode.Q))
                rotation = Quaternion.AngleAxis(-effectiveRotationSpeed, Vector3.up);
            if (Input.GetKey(KeyCode.E))
                rotation = Quaternion.AngleAxis(effectiveRotationSpeed, Vector3.up);

            Camera.main.transform.rotation = rotation * Camera.main.transform.rotation;

            if (Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.KeypadPlus))
                Camera.main.fieldOfView -= 0.5f;
            if (Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.KeypadMinus))
                Camera.main.fieldOfView += 0.5f;
        }

        private MapTile mouseoverTile = null;

        private void HandleMouseover()
        {
            MapTile tile = GetTileFromObject(Utils.GetMouseoverObject());

            if (tile != null && mouseoverTile != tile)
            {
                mouseoverTile = tile;

                InfoText.text = 
                            " Turn: " + gameLogic.TurnNumber +
                            "\n Mode: " + currentMode +
                            "\n" +
                            "\n X: " + tile.X + " Y: " + tile.Y +
                            "\n Height:     " + tile.Height +
                            "\n Forestation:" + tile.Properties.Forestation.ToString("F2") +
                            "\n Ruggedness: " + tile.Properties.Ruggedness.ToString("F2") +
                            "\n Temperature:" + tile.Properties.Temperature.ToString("F2") +
                            "\n Humidity:    " + tile.Properties.Humidity.ToString("F2");
            }
        }

        private void HandleMouse()
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
            {
                if (mouseoverTile != null)
                {
                    switch (currentMode)
                    {
                        case EditorMode.None:
                            HandleSelection();
                            break;
                        case EditorMode.River:
                            HandleRiverPlacement();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }

        private River currentRiver = null;
        private void HandleRiverPlacement()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (currentRiver == null)
                {
                    currentRiver = new River();
                    tiledMap.AddRiver(currentRiver);
                }

                if (currentRiver.RiverNodes.Count == 0 ||
                    tiledMap.IsNeigbour(tiledMap.GetTile(currentRiver.RiverNodes[currentRiver.RiverNodes.Count - 1]), mouseoverTile))
                {
                    currentRiver.RiverNodes.Add(mouseoverTile.GetPosition());
                    tiledMap.UpdateAllRivers();
                }

                graphics.RedrawRivers();
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (currentRiver != null)
                {
                    tiledMap.RemoveRiver(currentRiver);
                    currentRiver = null;
                }

                graphics.RedrawRivers();
            }
        }

        private void HandleSelection()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (mouseoverTile.Units.Count == 0 && Input.GetKey(KeyCode.LeftControl))
                {
                    gameLogic.CreateUnit(gameLogic.gameData.UnitTypes.ElementAt(Random.Range(0, gameLogic.gameData.UnitTypes.Count)).Value, mouseoverTile, gameLogic.gameData.Factions.ElementAt(0).Value);
                    selectedUnit = mouseoverTile.Units[0];
                }
                else if (mouseoverTile.Units.Count == 0 && Input.GetKey(KeyCode.LeftAlt))
                {
                    gameLogic.CreateUnit(gameLogic.gameData.UnitTypes.ElementAt(Random.Range(0, gameLogic.gameData.UnitTypes.Count)).Value, mouseoverTile, gameLogic.gameData.Factions.ElementAt(1).Value); 
                    selectedUnit = mouseoverTile.Units[0];
                }
                else if (mouseoverTile.Units.Count > 0)
                    selectedUnit = mouseoverTile.Units[0];
                else
                    selectedUnit = null;

                UpdateSelectionInterface();
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (selectedUnit != null)
                {
                    if (!Input.GetKey(KeyCode.LeftShift))
                    {
                        selectedUnit.OrdersList.Clear();
                        selectedUnit.CurrentOrder = null;
                    }

                    List<MapTile> path = tiledMap.FindPath(selectedUnit.CurrentTile, mouseoverTile);

                    if (path != null)
                    {
                        selectedUnit.OrdersList.Add(new Order
                        {
                            Source = selectedUnit, Target = mouseoverTile, Type = OrderType.Move
                        });
                        EventSystem.SendUnitEvent(EventSystem.UnitEventType.GivenOrders, selectedUnit);
                    }
                    else
                    {
                        MessageText.text = "No path from " + selectedUnit.CurrentTile.GetTilePosString() + " to " + mouseoverTile.GetTilePosString();
                    }
                }
            }
        }

        private void UpdateSelectionInterface()
        {
            graphics.SetSelectedUnit(selectedUnit);
        }

        private MapTile GetTileFromObject(GameObject gameObject)
        {
            if (gameObject == null)
                return null;

            IntVector2 WH = graphics.CoordToMapWH(gameObject.transform.position);

            if (WH.x < 0 || WH.x >= tiledMap.Width || WH.y < 0 || WH.y >= tiledMap.Height)
                return null;

            return tiledMap.Tiles[WH.x, WH.y];
        }
    }
}
