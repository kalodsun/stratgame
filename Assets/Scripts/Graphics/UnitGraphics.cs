﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Entities;
using UnityEngine;

namespace Assets.Scripts.Graphics
{
    class UnitGraphics
    {
        private readonly Unit unit;

        private readonly GameObject unitModel, unitModelBase, unitBase;
        private readonly UnitBanner banner;
        private readonly GameGraphics gameGraphics;

        public UnitGraphics(GameGraphics graphics, Unit unit)
        {
            this.gameGraphics = graphics;
            this.unit = unit;
            unitBase = new GameObject("Unit Base " + unit.Stats.Name);
            unitModelBase = new GameObject("Unit Model Base " + unit.Stats.Name);

            unitModel = graphics.ModelManager.GetModelObject(unit.Stats.ModelName);
            unitModel.transform.SetParent(unitModelBase.transform, false);
            unitModelBase.transform.SetParent(unitBase.transform, false);

            banner = new UnitBanner(unitBase, unit);
        }

        public void Update()
        {
            if (unit.Alive)
            {
                unitBase.transform.position =
                    gameGraphics.getTileGraphics(unit.CurrentTile).getSceneryBase().transform.position;
                unitModelBase.transform.rotation = Quaternion.AngleAxis((int) unit.Facing*-45f + 180, Vector3.up);

                banner.Update();

                unitBase.SetActive(unit.Alive);
            }
            else
            {
                unitBase.SetActive(false);
            }
        }

        public void Destroy()
        {
            GameObject.Destroy(unitBase);
        }
    }
}
