﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;
using Assets.Scripts.Game.Map;
using Assets.Scripts.Graphics.ResourceManagers;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Graphics
{
    public class GameGraphics {
        private TiledMap map;
        private readonly GameObject terrainElement, riverElement;
        private GameLogic gameLogic;
        private TileGraphics[,] tileGraphics = null;
        private Dictionary<Unit, UnitGraphics> unitGraphics = new Dictionary<Unit, UnitGraphics>();

        private static readonly GameObject PathMarker = Resources.Load<GameObject>("Prefabs/UI/PathMarker");
        private static readonly GameObject SelectionMarkerPrefab = Resources.Load<GameObject>("Prefabs/UI/PathMarker");

        private static readonly List<GameObject> PathMarkers = new List<GameObject>();
        private static readonly List<GameObject> RiverElements = new List<GameObject>();
        private readonly GameObject SelectionMarker;

        private readonly ModelManager modelManager;

        private readonly float tileSize, heightScale;

        public int Width { get; private set; }
        public int Height { get; private set; }


        public GameGraphics(TiledMap tiledMap, GameLogic gameLogic, GameObject terrainElement, GameObject riverElement, float tileSize, float heightScale)
        {
            this.map = tiledMap;
            this.gameLogic = gameLogic;
            this.heightScale = heightScale;
            this.terrainElement = terrainElement;
            this.riverElement = riverElement;
            this.tileSize = tileSize;

            MapColorManager.InitMaterials(map.MaxTileHeight, 10, terrainElement.GetComponent<Renderer>().sharedMaterial);
            SelectionMarker = GameObject.Instantiate(SelectionMarkerPrefab);
            SelectionMarker.SetActive(false);

            modelManager = new ModelManager(this);
        }

        public void ChangeMap(GameLogic gameLogic, TiledMap map)
        {
            this.gameLogic = gameLogic;
            this.map = map;

            foreach (KeyValuePair<Unit, UnitGraphics> unitGraphicse in unitGraphics)
            {
                unitGraphicse.Value.Destroy();
            }
            unitGraphics.Clear();

            foreach (TileGraphics tileGraphicse in tileGraphics)
            {
                tileGraphicse.Destroy();
            }

            MapColorManager.InitMaterials(map.MaxTileHeight, 10, terrainElement.GetComponent<Renderer>().sharedMaterial);
            GenerateTileGraphics(map.Width, map.Height);
            AssembleTiles();
            RedrawRivers();
            UpdateAllUnits();
        }

        public void GenerateTileGraphics(int w, int h)
        {
            Width = w;
            Height = h;

            tileGraphics = new TileGraphics[w, h];
            for(int i = 0;i < w; i++)
                for (int j = 0; j < h; j++)
                {
                    tileGraphics[i, j] = new TileGraphics(terrainElement, new Vector3(i, j), this);
                }
        }

        public void SetSelectedUnit(Unit unit)
        {
            if (unit == null)
            {
                SelectionMarker.SetActive(false);
                foreach (GameObject pathMarker in PathMarkers)
                {
                    pathMarker.SetActive(false);
                }
            }
            else
            {
                SelectionMarker.transform.position = tileGraphics[unit.CurrentTile.X, unit.CurrentTile.Y].sceneryBase.transform.position;
                SelectionMarker.SetActive(true);

                MapTile startTile = null;
                int pathMarkerIndex = 0;

                if (unit.CurrentOrder != null)
                {
                    if (unit.CurrentOrder.Type == OrderType.Move)
                    {
                        if (startTile == null)
                            startTile = unit.CurrentOrder.Source.CurrentTile;
                        List<MapTile> path = map.FindPath(startTile, unit.CurrentOrder.Target);

                        foreach (MapTile mapTile in path)
                        {
                            if (pathMarkerIndex == PathMarkers.Count)
                                PathMarkers.Add(GameObject.Instantiate(PathMarker));

                            PathMarkers[pathMarkerIndex].SetActive(true);
                            PathMarkers[pathMarkerIndex].transform.position = tileGraphics[mapTile.X, mapTile.Y].sceneryBase.transform.position;

                            pathMarkerIndex++;
                        }

                        startTile = unit.CurrentOrder.Target;
                    }
                }

                foreach (Order order in unit.OrdersList)
                {
                    if (order.Type == OrderType.Move)
                    {
                        if (startTile == null)
                            startTile = order.Source.CurrentTile;
                        List<MapTile> path = map.FindPath(startTile, order.Target);

                        foreach (MapTile mapTile in path)
                        {
                            if(pathMarkerIndex == PathMarkers.Count)
                                PathMarkers.Add(GameObject.Instantiate(PathMarker));

                            PathMarkers[pathMarkerIndex].SetActive(true);
                            PathMarkers[pathMarkerIndex].transform.position = tileGraphics[mapTile.X, mapTile.Y].sceneryBase.transform.position;

                            pathMarkerIndex++;
                        }

                        startTile = order.Target;
                    }
                }

                while (pathMarkerIndex < PathMarkers.Count)
                {
                    PathMarkers[pathMarkerIndex].SetActive(false);
                    pathMarkerIndex++;
                }
            }
        }

        public void UpdateAllUnits()
        {
            foreach (Unit unit in gameLogic.Units)
            {
                UpdateUnit(unit);
            }

        }

        public void UpdateUnit(Unit unit)
        {
            if(!unitGraphics.ContainsKey(unit))
                unitGraphics.Add(unit, new UnitGraphics(this, unit));

            unitGraphics[unit].Update();
        }

        public void RemoveUnit(Unit unit)
        {
            if (unitGraphics.ContainsKey(unit))
            {
                unitGraphics[unit].Destroy();
                unitGraphics.Remove(unit);
            }
        }

        public void UpdateAllTiles()
        {
            for(int x = 0;x < Width;x++)
                for(int y = 0;y < Height;y++)
                    tileGraphics[x, y].UpdateTerrain(map.Tiles[x, y]);
        }


        public void UpdateTile(MapTile tile)
        {
            UpdateTile(tile.X, tile.Y);
        }

        public void UpdateTile(int x, int y)
        {
            if (!Utils.IsValidCoord(x, map.Width) || !Utils.IsValidCoord(y, map.Height))
                throw new ArgumentOutOfRangeException("Given coordinates in UpdateTile are invalid: (" + x + ", " + y + ")");

            MapTile tile = map.Tiles[x, y];

            tileGraphics[x, y].UpdateTerrain(tile);
        }

        public void AssembleTiles()
        {           
            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                {
                    {
                        tileGraphics[x, y].UpdateTerrain(map.Tiles[x, y]);
                    }
                }
        }

        public void RedrawRivers()
        {
            int riverElementIndex = 0;

            for(int x = 0;x < map.Width;x++)
                for (int y = 0; y < map.Height; y++)
                {
                    if (riverElementIndex == RiverElements.Count)
                    {
                        RiverElements.Add(Object.Instantiate(riverElement));
                        RiverElements[riverElementIndex].transform.localScale *= tileSize; 
                        RiverElements[riverElementIndex].SetActive(false);
                    }

                    if (map.HorizontalEdges[x, y] == BorderType.River)
                    {
                        RiverElements[riverElementIndex].SetActive(true);
                        RiverElements[riverElementIndex].transform.position = tileGraphics[x, y].sceneryBase.transform.position + new Vector3(0, 0, -0.5f * tileSize);

                        float midHeight = (tileGraphics[x, y].sceneryBase.transform.position.y + tileGraphics[x, y-1].sceneryBase.transform.position.y)/2;
                        RiverElements[riverElementIndex].transform.position = new Vector3(RiverElements[riverElementIndex].transform.position.x, midHeight, RiverElements[riverElementIndex].transform.position.z);

                        RiverElements[riverElementIndex].transform.rotation = Quaternion.AngleAxis(0f, Vector3.up);
                        riverElementIndex++;
                    }



                    if (riverElementIndex == RiverElements.Count)
                    {
                        RiverElements.Add(Object.Instantiate(riverElement));
                        RiverElements[riverElementIndex].transform.localScale *= tileSize;
                        RiverElements[riverElementIndex].SetActive(false);
                    }

                    if (map.VerticalEdges[x, y] == BorderType.River)
                    {
                        RiverElements[riverElementIndex].SetActive(true);
                        RiverElements[riverElementIndex].transform.position = tileGraphics[x, y].sceneryBase.transform.position + new Vector3(-0.5f * tileSize, 0, 0);

                        float midHeight = (tileGraphics[x, y].sceneryBase.transform.position.y + tileGraphics[x-1, y].sceneryBase.transform.position.y) / 2;
                        RiverElements[riverElementIndex].transform.position = new Vector3(RiverElements[riverElementIndex].transform.position.x, midHeight, RiverElements[riverElementIndex].transform.position.z);

                        RiverElements[riverElementIndex].transform.rotation = Quaternion.AngleAxis(90f, Vector3.up);
                        riverElementIndex++;
                    }
                }

            for(;riverElementIndex < RiverElements.Count;riverElementIndex++)
                RiverElements[riverElementIndex].SetActive(false);
        }

        public TileGraphics getTileGraphics(MapTile tile)
        {
            return tileGraphics[tile.X, tile.Y];
        }

        public Vector3 getRandomPosInTile()
        {
            return new Vector3((Random.value - 0.5f) * tileSize, (Random.value - 0.5f) * tileSize, 0);
        }

        public IntVector2 CoordToMapWH(Vector3 coord)
        {
            return CoordToMapWH(new Vector2(coord.x, coord.z));
        }

        public IntVector2 CoordToMapWH(Vector2 coord)
        {
            return new IntVector2(coord/tileSize);
        }

        public IntVector2 CoordToGroundWH(Vector3 coord)
        {
            return CoordToGroundWH(new Vector2(coord.x, coord.z));
        }

        public IntVector2 CoordToGroundWH(Vector2 coord)
        {
            return new IntVector2(coord / tileSize);
        }

        public float TileSize
        {
            get { return tileSize; }
        }

        public float HeightScale
        {
            get { return heightScale; }
        }

        public ModelManager ModelManager
        {
            get { return modelManager; }
        }
    }
}
