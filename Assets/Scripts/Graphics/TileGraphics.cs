﻿using System;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Map;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Graphics
{
    public class TileGraphics
    {
        private readonly GameObject baseGameObject;
        private const float BASE_HEIGHT = 0.1f;

        private readonly GameGraphics graphics;
        private Vector3 position;

        public GameObject sceneryBase;

        public static float getSurfaceHeight(MapTile tile, GameGraphics graphics)
        {
            return BASE_HEIGHT + Mathf.Max(tile.Height, 0) * graphics.HeightScale;
        }

        public TileGraphics(GameObject baseGameObject, Vector2 pos, GameGraphics graphics)
        {
            position = new Vector3(pos.x * graphics.TileSize, 0, pos.y * graphics.TileSize);
            this.graphics = graphics;

            this.baseGameObject = Object.Instantiate(baseGameObject);
            this.baseGameObject.transform.position = position;
        }

        public void Destroy()
        {
            UpdateToEmpty();
            Object.Destroy(baseGameObject);
        }

        public void UpdateToEmpty()
        {
            if (sceneryBase != null)
            {
                ClearScenery();
                Object.Destroy(sceneryBase);
                sceneryBase = null;
            }

            baseGameObject.transform.localScale = new Vector3(graphics.TileSize, BASE_HEIGHT, graphics.TileSize);
            baseGameObject.GetComponent<Renderer>().sharedMaterial = MapColorManager.GetWaterMaterial();
        }

        public void UpdateTerrain(MapTile tile)
        {
            if (sceneryBase != null)
                ClearScenery();

            float height = getSurfaceHeight(tile, graphics);

            baseGameObject.transform.localScale = new Vector3(graphics.TileSize, height, graphics.TileSize);
            baseGameObject.GetComponent<Renderer>().sharedMaterial = MapColorManager.GetMaterial(tile.Height);

            
            if(sceneryBase == null)
                sceneryBase = new GameObject();

            sceneryBase.name = "SceneryBase " + position.x + " " + position.y;
            sceneryBase.transform.position = position + new Vector3(0, height/2, 0);

            graphics.ModelManager.CreateForest(tile.Properties, sceneryBase, tile.X + tile.Y);

            foreach (var structure in tile.Structures)
            {
                GameObject structureObject = graphics.ModelManager.GetModelObject(structure.Stats.ModelName);
                structureObject.transform.parent = sceneryBase.transform;
                structureObject.transform.localPosition = Vector3.zero;

                if (structure.Stats.Directional)
                {
                    structureObject.transform.localRotation *= Quaternion.AngleAxis(-45 * (int)structure.Direction, Vector3.up);
                }
            }
        }

        private void ClearScenery()
        {
            for (int i = sceneryBase.transform.childCount-1; i >= 0; i--)
            {
                GameObject child = sceneryBase.transform.GetChild(i).gameObject;

                if (graphics.ModelManager.IsModel(child))
                    graphics.ModelManager.ReturnModelObject(child);
                else
                    Object.Destroy(child);
            }
        }

        public GameObject getSceneryBase()
        {
            return sceneryBase;
        }
    }
}
