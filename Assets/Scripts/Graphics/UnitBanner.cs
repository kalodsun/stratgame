﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Entities;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Graphics
{
    class UnitBanner
    {
        private static GameObject UICanvas;
        private static readonly GameObject UnitCanvas = Resources.Load<GameObject>("Prefabs/UI/UnitCanvas");

        private readonly Unit unit;
        private readonly GameObject canvas;
        private readonly Slider strengthBar, organizationBar;
        private readonly Text nameText;

        public UnitBanner(GameObject unitObject, Unit unit)
        {
            if (UICanvas == null)
                UICanvas = GameObject.Find("UICanvas");

            this.unit = unit;
            canvas = Object.Instantiate(UnitCanvas);

            strengthBar = canvas.transform.Find("StrengthBar").gameObject.GetComponent<Slider>();
            organizationBar = canvas.transform.Find("OrganizationBar").gameObject.GetComponent<Slider>();
            nameText = canvas.transform.Find("NameText").gameObject.GetComponent<Text>();

            canvas.transform.SetParent(unitObject.transform, false);
            canvas.transform.localPosition = new Vector3(0, 2, 0);

            Update();
        }

        public void Update()
        {
            Color factionColor = Utils.GetColorFromUint(unit.Faction.Color);
            factionColor.a = 0.4f;
            canvas.GetComponent<Image>().color = factionColor;
            nameText.text = unit.Stats.VisibleName;
            strengthBar.value = (float)unit.Properties.Strength / unit.Stats.MaxStrength;
            organizationBar.value = unit.Properties.Organization / unit.Stats.MaxOrganization;
        }

        public void Destroy()
        {
            GameObject.Destroy(canvas);
        }
    }
}
