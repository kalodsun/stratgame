﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Map;
using UnityEngine;
using UnityEngine.Rendering;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Graphics.ResourceManagers
{
    public class ModelManager
    {
        private const string TREE_DATA_PATH = "ResourceData/TreeData";
        private const string TREE_PREFAB_DIR = "Prefabs/Trees/";
        private const string TREE_MATERIAL_DIR = "Materials/Trees/";

        private const string STRUCTURE_DATA_PATH = "ResourceData/StructureData";
        private const string STRUCTURE_PREFAB_DIR = "Prefabs/Structures/";
        private const string STRUCTURE_MATERIAL_DIR = "Materials/Structures/";

        private const string UNIT_DATA_PATH = "ResourceData/UnitData";
        private const string UNIT_PREFAB_DIR = "Prefabs/Units/";
        private const string UNIT_MATERIAL_DIR = "Materials/Units/";

        private const int MAX_TREE_DENSITY = 30;

        private readonly GameGraphics graphics;

        private readonly List<TreeData> treeTypes = new List<TreeData>();
        private readonly List<StructureData> structureTypes = new List<StructureData>();
        private readonly List<UnitData> unitTypes = new List<UnitData>(); 

        private readonly Dictionary<string, LinkedList<GameObject>> modelPool = new Dictionary<string, LinkedList<GameObject>>();

        public ModelManager(GameGraphics graphics)
        {
            this.graphics = graphics;

            ReadTreeData();
            ReadStructureData();
            ReadUnitData();
        }

        private void ReadTreeData()
        {
            var dataAsset = Resources.Load<TextAsset>(TREE_DATA_PATH);
            var reader = new StringReader(dataAsset.text);

            TreeData currentData = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0][0] == '#')
                    continue;

                if (currentData == null)
                {
                    currentData = new TreeData
                    {
                        PrototypeObject = Resources.Load<GameObject>(TREE_PREFAB_DIR + words[0]),
                        MinTemp = float.Parse(words[1]),
                        MaxTemp = float.Parse(words[2]),
                        Skins = new List<TreeData.TreeSkin>()
                    };

                    modelPool.Add(currentData.PrototypeObject.name, new LinkedList<GameObject>());
                }
                else
                {
                    if (words[0] == "{")
                        continue;

                    if (words[0] == "}")
                    {
                        treeTypes.Add(currentData);
                        currentData = null;
                    }
                    else
                    {
                        Material cMat = Resources.Load<Material>(TREE_MATERIAL_DIR + words[0]);
                        currentData.Skins.Add(new TreeData.TreeSkin(cMat, float.Parse(words[1]), float.Parse(words[2])));
                    }
                }
            }

            Resources.UnloadAsset(dataAsset);
        }

        private void ReadStructureData()
        {
            var dataAsset = Resources.Load<TextAsset>(STRUCTURE_DATA_PATH);
            var reader = new StringReader(dataAsset.text);

            StructureData currentData = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (currentData == null)
                {
                    currentData = new StructureData()
                    {
                        PrototypeObject = Resources.Load<GameObject>(STRUCTURE_PREFAB_DIR + words[0]),
                        Skins = new List<StructureData.StructureSkin>()
                    };

                    modelPool.Add(currentData.PrototypeObject.name, new LinkedList<GameObject>());
                }
                else
                {
                    if (words[0] == "{")
                        continue;

                    if (words[0] == "}")
                    {
                        structureTypes.Add(currentData);
                        currentData = null;
                    }
                    else
                    {
                        Material cMat = Resources.Load<Material>(STRUCTURE_MATERIAL_DIR + words[0]);
                        currentData.Skins.Add(new StructureData.StructureSkin(cMat));
                    }
                }
            }

            Resources.UnloadAsset(dataAsset);
        }

        private void ReadUnitData()
        {
            var dataAsset = Resources.Load<TextAsset>(UNIT_DATA_PATH);
            var reader = new StringReader(dataAsset.text);

            UnitData currentData = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0][0] == '#')
                    continue;

                if (currentData == null)
                {
                    currentData = new UnitData()
                    {
                        PrototypeObject = Resources.Load<GameObject>(UNIT_PREFAB_DIR + words[0]),
                        Skins = new List<UnitData.UnitSkin>()
                    };

                    modelPool.Add(currentData.PrototypeObject.name, new LinkedList<GameObject>());
                }
                else
                {
                    if (words[0] == "{")
                        continue;

                    if (words[0] == "}")
                    {
                        unitTypes.Add(currentData);
                        currentData = null;
                    }
                    else
                    {
                        Material cMat = Resources.Load<Material>(UNIT_MATERIAL_DIR + words[0]);
                        currentData.Skins.Add(new UnitData.UnitSkin(cMat));
                    }
                }
            }

            Resources.UnloadAsset(dataAsset);
        }

        public bool IsModel(GameObject gameObject)
        {
            return modelPool.ContainsKey(gameObject.name);
        }

        public void ReturnModelObject(GameObject modelObject)
        {
            if (IsModel(modelObject))
            {
                modelObject.transform.parent = null;
                modelObject.SetActive(false);
                modelPool[modelObject.name].AddLast(modelObject);
            }
            else
            {
                throw new ArgumentException("Object given for return is not a valid model");
            }
        }

        private GameObject GetModelObject(GameObject prototype)
        {
            if (modelPool.ContainsKey(prototype.name))
            {
                GameObject ret;
                if (modelPool[prototype.name].Count > 0)
                {
                    ret = modelPool[prototype.name].Last.Value;
                    ret.SetActive(true);
                    modelPool[prototype.name].RemoveLast();
                }
                else
                {
                    ret = Object.Instantiate(prototype);
                }

                ret.name = prototype.name;

                return ret;
            }
            else
            {
                throw new ArgumentException("Object asked for is not a valid model");
            }
        }

        public GameObject GetModelObject(string name)
        {
            GameObject ret = tryGetStructure(name);

            if (ret == null)
                ret = tryGetUnit(name);

            return ret;
        }

        private GameObject tryGetStructure(string name)
        {
            StructureData structure = null;

            foreach (var structureType in structureTypes)
            {
                if (structureType.PrototypeObject.name != name)
                    continue;

                structure = structureType;
                break;
            }

            if (structure == null)
                return null;

            GameObject ret = GetModelObject(structure.PrototypeObject);
            ret.GetComponentInChildren<Renderer>().sharedMaterial = structure.Skins[0].Material;
            return ret;
        }

        private GameObject tryGetUnit(string name)
        {
            UnitData unit = null;

            foreach (var unitType in unitTypes)
            {
                if (unitType.PrototypeObject.name != name)
                    continue;

                unit = unitType;
                break;
            }

            if (unit == null)
                return null;

            GameObject ret = GetModelObject(unit.PrototypeObject);
            ret.GetComponent<Renderer>().sharedMaterial = unit.Skins[0].Material;
            return ret;
        }

        public void CreateForest(MapTile.PropertiesCls properties, GameObject parentObject, int seed)
        {
            Random.InitState(seed);
            CreateForest(properties, parentObject);
        }

        public void CreateForest(MapTile.PropertiesCls properties, GameObject parentObject)
        {
            int treeTarget = (int)(properties.Forestation * MAX_TREE_DENSITY);

            var suitableTrees = new List<TreeData>();

            foreach (var type in treeTypes)
            {
                if (type.MinTemp <= properties.Temperature && type.MaxTemp >= properties.Temperature)
                    suitableTrees.Add(type);
            }

            if(suitableTrees.Count == 0)
                return;

            int[] treeCoutns = Utils.SplitInRandomParts(treeTarget, suitableTrees.Count);

            var suitableSkins = new List<TreeData.TreeSkin>();
            for (int i = 0; i < treeCoutns.Length; i++)
            {
                foreach (TreeData.TreeSkin skin in suitableTrees[i].Skins)
                {
                    if (skin.MinTemp <= properties.Temperature && skin.MaxTemp >= properties.Temperature)
                        suitableSkins.Add(skin);
                }

                if(suitableSkins.Count == 0)
                    return;

                int[] skinCounts = Utils.SplitInRandomParts(treeCoutns[i], suitableSkins.Count);

                for (int j = 0; j < skinCounts.Count(); j++)
                {
                    CombineInstance[] combineInstances = new CombineInstance[skinCounts[j]];
                    GameObject combinedTrees = new GameObject();
                    combinedTrees.AddComponent<MeshFilter>();
                    combinedTrees.GetComponent<MeshFilter>().mesh = new Mesh();

                    for (int k = 0; k < skinCounts[j]; k++)
                    {
                        /*GameObject tree = GetModelObject(suitableTrees[i].PrototypeObject);

                        tree.GetComponent<Renderer>().sharedMaterial = suitableSkins[j].Material;
                        tree.transform.parent = parentObject.transform;
                        tree.transform.localPosition = graphics.getRandomPosInTile();*/

                        combineInstances[k].mesh = suitableTrees[i].PrototypeObject.GetComponent<MeshFilter>().sharedMesh;
                        combineInstances[k].transform = suitableTrees[i].PrototypeObject.transform.localToWorldMatrix * Matrix4x4.Translate(graphics.getRandomPosInTile());
                    }

                    combinedTrees.GetComponent<MeshFilter>().mesh.CombineMeshes(combineInstances);
                    combinedTrees.SetActive(true);
                    combinedTrees.transform.SetParent(parentObject.transform, false);
                    combinedTrees.AddComponent<MeshRenderer>();
                    combinedTrees.GetComponent<MeshRenderer>().sharedMaterial = suitableSkins[j].Material;
                    combinedTrees.GetComponent<MeshRenderer>().shadowCastingMode = ShadowCastingMode.Off;
                }

                suitableSkins.Clear();
            }
        }
    }

    class TreeData
    {
        public List<TreeSkin> Skins;
        public GameObject PrototypeObject;

        public float MinTemp, MaxTemp;

        public class TreeSkin
        {
            public readonly Material Material;
            public readonly float MinTemp, MaxTemp;

            public TreeSkin(Material mat, float min, float max)
            {
                Material = mat;
                MinTemp = min;
                MaxTemp = max;
            }
        }
    }

    class StructureData
    {
        public List<StructureSkin> Skins;
        public GameObject PrototypeObject;

        public class StructureSkin
        {
            public readonly Material Material;

            public StructureSkin(Material mat)
            {
                Material = mat;
            }
        }
    }

    class UnitData
    {
        public List<UnitSkin> Skins;
        public GameObject PrototypeObject;

        public class UnitSkin
        {
            public readonly Material Material;

            public UnitSkin(Material mat)
            {
                Material = mat;
            }
        }
    }
}
