﻿using System;
using UnityEngine;

namespace Assets.Scripts.Graphics
{
    static class MapColorManager
    {
        private static Material[] materials = null;
        private static Material waterMaterial = null;
        private static int materialStep;
        private static readonly Color WaterColor = new Color(9f / 255, 134f / 255, 200f / 255);

        public static void InitMaterials(int maxHeight, int step, Material baseMaterial)
        {
            waterMaterial = new Material(baseMaterial) {color = WaterColor};

            materialStep = step;
            materials = new Material[maxHeight / step + 1];

            for (int i = 0; i <= maxHeight / step; i++)
            {
                materials[i] = new Material(baseMaterial);
                materials[i].color = GetColor(i * step, maxHeight);
            }
        }

        public static Material GetMaterial(int height)
        {
            if (materials == null)
                throw new InvalidOperationException("GetMaterial called before InitMaterials");

            if (height < 0)
                return GetWaterMaterial();

            if (height > materials.Length * materialStep)
                return materials[materials.Length - 1];

            return materials[height / materialStep];
        }

        public static Material GetWaterMaterial()
        {
            if (waterMaterial == null)
                throw new InvalidOperationException("GetWaterMaterial called before InitMaterials");

            return waterMaterial;
        }

        public static Color GetColor(int height, int maxHeight)
        {
            if (height <= -1)
                return WaterColor;

            float heightPercentage = ((float)height) / maxHeight;
            return new Color(GetR(heightPercentage), GetG(heightPercentage), GetB(heightPercentage));
        }

        private static float GetR(float percentage)
        {
            return 144f / 255 + percentage / 0.4f;
        }

        private static float GetG(float percentage)
        {
            return (-0.03f * (percentage * 100 - 33) * (percentage * 100 - 33) + 255) / 255f;
        }

        private static float GetB(float percentage)
        {
            return (-0.035f * (percentage * 100 - 33) * (percentage * 100 - 33) + 189) / 255f;
        }
    }
}